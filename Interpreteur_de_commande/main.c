#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "Fonctions_utiles.h"
#include "Commandes.h"

int main()
{
    char profile_file_path[1024];
    FILE *profile_file = NULL;
    char tmp[1024];
    char *var_name;
    char *var_value;
    int var_env = 0;
    char *home;
    char *path_dir;
    char path[10][100];  // Tableau de string contenant les chemins qu'on lit à partir du fichier "profile".
    int path_last_index =-1;
    char *rep_courant;
    int argc ; // Nbr d'élements dans une instruction
    char *argv[100] ;
    FILE *file_cmd = NULL;
    
    // Menu d'acceuil 
    printf("\n");
    printf("\t ========= BIENVENUE  ========= \n \n");
    printf("Veuillez choisir une partie: (1,2) \n \n");
    printf("1. Partie 1: Interpréteur de commande \n");
    printf("2. Partie 2: Simulateur d'ordonnacement des processus \n");
    printf("\n--> ");
    
    char *choix;
    choix = (char*)malloc(1024*sizeof(char));
    scanf("%[^\n]%*c", choix);
    
    if (!strcmp(choix, "1")){


    // Récupérer le fichier "profile"
    if (getcwd(profile_file_path, sizeof(profile_file_path)) != NULL) // Cette fonction permet de récupérer le répertoire courant.
    {
        strcat(profile_file_path,"/profile.txt");
        profile_file = fopen(profile_file_path,"r");
    }

    // Lecture de toutes les lignes du fichier "profile".
    if (profile_file != NULL)
    {
        while (1)
        {
            fscanf(profile_file, "%s", &tmp);
            if (tmp[0] == NULL) // Si on lit une ligne vide, on sort de la boucle while.
                break;

            var_name = strtok(tmp,"=");   // on récupére la partie de la chaine avant "="
            var_value = strtok(NULL,"="); // on récupére la partie de la chaine aprés "="

            if ( !strcmp(var_name, "HOME"))
            {
                var_env++;
                home = var_value;
            }
            else if ( !strcmp(var_name, "PATH"))
            {
                var_env++;
                path_dir = strtok(var_value, ":");
                path_last_index = -1;

                while(path_dir != NULL)
                {
                    char str[50];

                    path_last_index++;
                    strcpy(str, path_dir);
                    strcpy(path[path_last_index], str);
                    path_dir = strtok(NULL, ":");

                }
             }
         tmp[0] = NULL;
        }
      fclose(profile_file);
    }

    if (var_env < 2)
    {
        printf("Erreur:_Fichier Profile: Variable(s) d'environnement manquante(s).\n");
        return -1;
    }

    if (!existe(home))
    {
        printf("HOME:_Chemin n'existe pas.\n");
        return -1;
    }
    rep_courant = home;
    chdir(rep_courant);

    char *instruction;
    instruction = (char*)malloc(1024*sizeof(char));

    do
    {
        printf("\n%s>",rep_courant);  // Afficher ">/home"
        scanf("%[^\n]%*c", instruction); // Lire une chaine de caractères avec des espaces

        // On divise l'instruction sous la forme "commande + path"
        char *strtoken = strtok(instruction, " ");
        argc = -1;
        while (strtoken != NULL)
        {
            argc++;
            argv[argc] = strtoken;
            strtoken = strtok(NULL, " ");
        }

        char *commande = argv[0];

        if(!strcmp(commande,"my_pwd"))
            my_pwd(argc, argv);
        else if (!strcmp(commande,"my_cd"))
            my_cd(argc, argv, &rep_courant);
        else if (!strcmp(commande, "my_exec")) 
        {
            file_cmd = fopen(argv[1], "r");

             if (file_cmd != NULL)
             {
                while(1)
                {
                    fscanf(file_cmd, "%[^\n]%*c", &tmp);
                    if (tmp[0]==NULL) break;
                    system(tmp);
                    tmp[0] = NULL;
                }
             }
            fclose(file_cmd);
        }
        else
        {    // Si la commande tapée n'est pas une commande interne.
             if (access(commande, F_OK) == 0)
                 printf("Le programme %s existe.\n",commande);
             else
             { // Commande propre au systéme
                int o = 0;
                char str[40];
                while(o<=path_last_index)
                {
                    strcpy(str, path[o]);
                    strcat(str, "/");
                    strcat(str, commande);

                      if( access( str, F_OK ) != -1 )
                         break;
                      else
                        o++;
                }
                if (o<=path_last_index)
                {
                    int cc = 1;
                    while(argv[cc]!= NULL){
                        strcat(str, " ");
                        strcat(str, argv[cc]);
                        cc++;
                    }
                    system(str);
                }else
                    printf("la commande n'existe pas...");
             }
        }

    }while(!(!strcmp(argv[0],"my_exit")));

    }else (!strcmp (choix, "2")){
        
        char *arguments[] = { "OS_project", NULL };

        /* On lance le programme  */
        if (execv("/home/khalil/simulateur_ordonnancement_proccesus", arguments) == -1)
        {
    	    perror("execv");
            return EXIT_FAILURE;
            
        } else return EXIT_SUCCESS;

    }
    
    return 0;
}
