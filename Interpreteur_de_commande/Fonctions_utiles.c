#include "Fonctions_utiles.h"

int existe(const char *file_path)
{
    int e;
    struct stat buf;

    e = stat(file_path, &buf);
    if (e==0 && S_ISDIR(buf.st_mode))
        return 1;
    else
        return 0;

}
