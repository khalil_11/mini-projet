#include "Commandes.h"

void my_pwd(int argc, char *argv[])
{
    char buf[1024];

    if (argc < 1)
    {
        if (getcwd(buf, sizeof(buf)) != NULL)
            printf("Répertoire courant: %s\n", buf);
    }
    else
            printf("la commande ne nécessite aucun paramétre.\n");

}

void my_cd(int argc, char *argv[], char *rep_courant)
{
    int verif;

    if (argc < 2)
    {
        if (argc == 0)
        {
            verif = chdir(rep_courant);
            if (verif == 0)
            {
                chdir(rep_courant);
                printf("%s\n",rep_courant);
            }
            else
                printf("Le changement du répertoire à échoué.\n");
        }
        else
        {
            verif = chdir(argv[1]);
            if (verif == 0)
            {
                rep_courant = argv[1];
                printf("%s", rep_courant);
                chdir(rep_courant);
            }
            else if (!existe(argv[1]))
                printf("Le chemin n'existe pas.\n");
            else
                fprintf(stderr,"Erreur.\n");
        }
    }
    else
        printf("Trop d'arguments, veuillez entrez un chemin complet sans espace.\n");

}

